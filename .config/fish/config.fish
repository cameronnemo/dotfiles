set fish_color_user green
set fish_color_host red

set -x PGPASSFILE "$HOME/.config/postgresql/pgpass"
set -x PSQLRC "$HOME/.config/postgresql/psqlrc"
set -x PSQL_HISTORY "$HOME/.cache/postgresql/psql_history"
set -x NODE_REPL_HISTORY "$HOME/.cache/node_repl_history"
set -x REGISTRY_AUTH_FILE "$HOME/.cache/containers/auth.json"

if status --is-interactive;
    set fish_greeting ''
    stty intr '^X'
    set -x PATH /usr/local/bin /usr/bin ~/.local/bin ~/.cargo/bin
    set -x EDITOR nvim
    alias 'xup' 'sudo ~/.local/bin/xup'
    alias 'pwr' 'sudo ~/.local/bin/poweroff'
    alias 'tar' 'bsdtar'
    alias 'psql' '/usr/lib/psql15/bin/psql'
    alias 'syncdot' 'syncdotfiles-togit ~/src/dotfiles'
    alias 'syncdotwet' 'set cwd (pwd) ; cd ~/src/dotfiles ; syncdotfiles-togit-wet . ; git add -A ; git commit -v ; git push ; cd "$cwd"; set -e cwd'
    alias 'subk' '~/src/sub/gosocks kubectl'
    alias 'subtk' '~/src/sub/gosocks tk'
    function frun -a app
        start flatpak-run app=$app
    end
    complete -f -c frun -a "(flatpak list --app --columns=application,name)"
    function prompt_login
        :
    end
end

if test (tty) = "/dev/tty1"; and test -z "$DISPLAY"; and test -z "$WAYLAND_DISPLAY";
    set -x DESKTOP_SESSION 'sway'
    set -x XDG_CURRENT_DESKTOP 'sway'
    set -x XDG_SESSION_TYPE 'wayland'
    # set -x WLR_NO_HARDWARE_CURSORS 1
    exec startup --user
end
