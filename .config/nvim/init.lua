-- Install packer
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.fn.execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
end

local augroup = function(name)
  vim.api.nvim_create_augroup(name, { clear = true })
end
local packer_augroup = augroup('Packer')
vim.api.nvim_create_autocmd('BufWritePost', {
  pattern = 'init.lua',
  group = packer_augroup,
  command = 'PackerCompile',
})

require('packer').startup(function(use)
  use 'wbthomason/packer.nvim' -- Package manager
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
  use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp
  use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp
  use 'L3MON4D3/LuaSnip' -- Snippets plugin
  use 'google/vim-jsonnet'
  use 'cstrahan/vim-capnp'
  use 'fatih/vim-go'
  use 'othree/html5.vim'
  use 'pangloss/vim-javascript'
  use 'leafOfTree/vim-svelte-plugin'
end)

vim.opt.mouse = ''
vim.opt.number = true
vim.cmd('highlight LineNr ctermfg=grey')
-- XXX: does not work for whatever reason, maybe needs to be in an autocmd?
-- vim.api.nvim_set_hl(0, 'LineNr', { ctermfg = 'grey' })

-- TODO: Go, Rust, SQL
local languages = {
  lua = 2, yaml = 2, json = 2, jsonnet = 2,
  fish = 4, python = 4, cpp = 4, yang=4,
  html = 4, css = 4, javascript = 4, typescript = 4, svelte = 4,
}
local lang_augroup = augroup('LangIndent')
for lang, nindent in pairs(languages) do
  vim.api.nvim_create_autocmd('FileType', {
    pattern = lang,
    group = lang_augroup,
    callback = function()
      vim.opt.shiftwidth = nindent
      vim.opt.softtabstop = nindent
      vim.opt.expandtab = true
      vim.opt.autoindent = true
    end
  })
end

vim.cmd [[
  " jump to last position when reopening
  autocmd BufReadPost *
    \ if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' &&
    \ line("'\"") > 1 && line("'\"") <= line("$")
    \ | exe "normal! g`\""
    \ | endif
]]

vim.g.markdown_fenced_languages = {
  'python', 'sh', 'js=javascript', 'json', 'yml=yaml', 'yaml'
}

--
-- COMPLETIONS from https://github.com/neovim/nvim-lspconfig/wiki/Autocompletion
--

-- Add additional capabilities supported by nvim-cmp
local capabilities = require('cmp_nvim_lsp').default_capabilities({})

local lspconfig = require('lspconfig')

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = { 'rust_analyzer' , 'gopls' }
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    capabilities = capabilities,
  }
end
lspconfig.eslint.setup {
  capabilities = capabilities,
  filetypes = { 
    "javascript", "typescript", "svelte"
  }
}
lspconfig.jsonnet_ls.setup {
  capabilities = capabilities,
  cmd = {'jsonnet-language-server', '-t', '--lint'}
}
lspconfig.pylsp.setup {
  capabilities = capabilities,
  settings = {
    pylsp = {
      plugins = {
        ruff = {
          enabled = true,
        },
      },
    },
  },
}

-- luasnip setup
local luasnip = require 'luasnip'

-- nvim-cmp setup
local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end,
    ['<S-Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end,
  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },
}
