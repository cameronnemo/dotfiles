#!/bin/sh

set -e

export BEMENU_OPTS='
 -p sopen -l 10 --fn "Droid Sans Mono Bold" 13
 --tb #7B9E4B --tf #000000
 --fb #000000 --ff #7B9E4B
 --nb #000000 --nf #7B9E4B
 --ab #000000 --af #7B9E4B
 --hb #7B9E4B --hf #000000
 --cb #7B9E4B --cf #000000'

export BEMENU_BACKEND='wayland'

list_path_executables() {
	printf '%s' "$1" | tr ':' '\0' | \
		xargs -0 -I{} find {} -executable -printf '%f\n' | \
		sort -u
}

prog="$(list_path_executables "$PATH" | bemenu)"

sopen $prog
