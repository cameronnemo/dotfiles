#!/usr/bin/python3

import json

def one_term():
    return {'from': {'protocol': 'static'}, 'then': 'reject'}

def many_terms():
    return [
        {'from': {'protocol': 'babel'}, 'then': 'accept'},
        {'from': {'protocol': 'babel'}, 'then': 'accept'},
    ]

print(json.dumps([
    one_term(),
    *many_terms(),
], indent=2))
