"""https://www.dummies.com/food-drink/special-diets/glycemic-index-diet/how-to-measure-your-metabolic-rate/"""

def bmr(weight, height, age):
    return 66 + (6.23 * weight) + (12.7 * height) - (6.76 * age)

BMR = bmr(130, 6 * 12 + 1, 24)

print(BMR)

from pprint import pprint
pprint({
    '0 no': BMR * 1.2,
    '1 light': BMR * 1.375,
    '2 moderate': BMR * 1.55,
    '3 heavy': BMR * 1.725,
    '4 very heavy': BMR * 1.9,
})
