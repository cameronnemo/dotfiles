#!/bin/sh

set -eu

USER="$1"
HOST="$2"

configdir="${XDG_CONFIG_HOME:-$HOME/.config}/oconnect"
args=$(cat "${configdir}/args" 2>/dev/null)
cookiedir="${XDG_CACHE_HOME:-$HOME/.cache}/oconnect"
cookiefile="${cookiedir}/${USER}@${HOST}.cookie"

mkdir -p "$cookiedir"
test -s "$cookiefile" || \
	eval openconnect --cookieonly $args --user="$USER" "$HOST" \
	> "$cookiefile"

exec sudo start oconnect "USER=$USER" "HOST=$HOST" "COOKIEFILE=$cookiefile"
