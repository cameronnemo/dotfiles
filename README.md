# dotfiles

Configuration files and scripts for my configuration and scripts.
Driven by rsync and carefully crafted filter rules.

## fromgit

To initialize a home directory from this repo, first do a dry run:

```sh
./bootstrap/sync-fromgit .
```

Use the wet command to actually sync the files:

```sh
./bootstrap/sync-fromgit-wet .
```

## togit

The `~/.local/bin/syncdotfiles-togit` script is used to sync changes from the
home directory to a cloned git repo.

It takes a single argument, which is the local git repo destination.

The script has a -wet variant and uses the same wet/dry semantics as fromgit.

## customization and implementation details

Files to be synced are specified using rsync filter rule files.

For togit: `~/.config/syncdotfiles/{include,togit.rules}`

For fromgit: `./bootstrap/{include,fromgit.rules}`

But you should be aware that `./bootstrap/include` is symlinked to
`./.config/syncdotfiles/include`, i.e. togit and fromgit share that file.

If a file matches a pattern in `include` and is present on the sender but not
receiver side, it will be deleted.

For the togit script, if a file is not matched by the include rules, it will be
be deleted from the git repo.

`togit.rules` and `fromgit.rules` are used to make exceptions to those rules.
